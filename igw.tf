resource "aws_internet_gateway" "feb_23_vpc_igw" {
  vpc_id = aws_vpc.feb_23_vpc.id

  tags = {
    Name = "feb-23-vpc-igw"
  }
}
