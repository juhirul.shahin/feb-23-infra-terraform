resource "aws_eip" "nat_gw_eip" {
  domain   = "vpc"

  tags = {
    Name = "feb-23-vpc-natgw-eip"
  }
}


resource "aws_nat_gateway" "feb_23_vpc_natgw" {
  allocation_id = aws_eip.nat_gw_eip.id
  subnet_id     = aws_subnet.public_subnet_1.id

  tags = {
    Name = "feb-23-vpc-natgw"
  }

  depends_on = [aws_internet_gateway.feb_23_vpc_igw]
}
